
# Molecule Demo Project

This project is for documenting and testing development of Ansible roles using the Molecule framework.  Molecule is a tool for developing and testing Ansible roles.  You can find out more about Molecule using the links below.

Project: https://github.com/ansible-community/molecule

Docs: https://molecule.readthedocs.io/en/latest/

Tutorial: https://github.com/ansible-community/molecule (possibly outdated?)

## Quickstart for Molecule with Vagrant

1. create a new Ansible role project with Molecule using the Vagrant driver
1. remove unnecessary boilerplate files
```bash
molecule init role <role-name> -d vagrant
cd <role-name>
rm -rf .travis.yml tests/ molecule/default/INSTALL.rst
```

## Environments

While several drivers are supported, we will be focused on using Molecule with Vagrant driver.  See the Demo/Vagrant project for details on installing the tools required for the Vagrant driver: https://gitlab.davenport.edu/demo/vagrant

We are also looking at the podman driver.  There are currently 2 scenarios, one for the vagrant driver and the other for the podman driver.  You can run either by using the `-s` parameter to molecule commands:

```bash
molecule create -s vagrant
molecule create -s podman
```

### Linux / Vagrant / Libvirt

Run the following to install the dependencies with Ansible:

```bash
ansible-playbook local-setup.yml [--check]
```

Using the --check flag will run the playbook in check mode, showing what would be changed but not make any changes.

### Other Environments?


## Quick Start

Create a new role (uses `ansible-galaxy role init` under the hood)
```bash
molecule init role -d vagrant <rolename>
cd <rolename>
nano molecule/default.molecule.yml
molecule test
```
See the docs for more info on configuring the new Molecule-based role, specifically here for an overview: https://molecule.readthedocs.io/en/latest/getting-started.html

## Misc. Notes

Molecule creates the necessary Vagrant & Ansible files in ~/.cache/molecule/<project>/<scenario>, which means you can inspect the Vagrantfile and run vagrant commands from that directory.  You can also run ansible commands using the molecule-created inventory file:
```bash
ansible all -m setup \
-i ~/.cache/molecule/<project>/<scenario>/inventory/ansible_inventory.yml
```
